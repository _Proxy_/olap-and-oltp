SELECT -- Selecting product category and calculating the total sales amount
    p.prod_category, -- Selecting the product category
    SUM(s.amount_sold) AS total_sales_amount -- Calculating the total sales amount
FROM
    sh.sales s                                  -- Joining with the sales table
    JOIN sh.products p ON s.prod_id = p.prod_id -- Joining with the products table
    JOIN sh.times t ON s.time_id = t.time_id    -- Joining with the times table
WHERE
    t.time_id BETWEEN '1998-04-10' AND '1999-02-04' -- Filtering by the specified time period
GROUP BY
    p.prod_category;                            -- Grouping the result by product category

--This query selects the product category and calculates the total sales amount for each category within the specified time period.


-----------------------------------------------
   -- Selecting country region and calculating the average sales quantity
SELECT co.country_region,   -- Selecting the country region
       AVG(s.quantity_sold) AS average_sales_quantity   -- Calculating the average sales quantity
FROM sh.sales s                                         -- Joining with the sales table
JOIN sh.customers cu ON s.cust_id = cu.cust_id          -- Joining with the customers table
JOIN sh.countries co ON cu.country_id = co.country_id   -- Joining with the countries table
JOIN sh.products p ON s.prod_id = p.prod_id             -- Joining with the products table
WHERE p.prod_id = 22                                    -- Filtering by the specified product ID
GROUP BY co.country_region;                             -- Grouping the result by country region


SELECT
    c.country_region_id,    -- Selecting the country region id
    c.country_region,       -- Selecting the country region
    AVG(s.quantity_sold) AS avg_quantity_sold   -- Calculating the average sales quantity
FROM
    sh.countries c                                      -- Joining with the contries table
JOIN
    sh.customers cust ON c.country_id = cust.country_id -- Joining with the customers table
JOIN
    sh.sales s ON cust.cust_id = s.cust_id              -- Joining with the sales table
GROUP BY
    c.country_region_id, c.country_region               -- Grouping the result by country region id and name
ORDER BY
    avg_quantity_sold ;                                 -- Ordering by average quantity

--This queries selects the country region and calculates the average sales quantity for a specified product within each region.
------------------------------------------------
   
-- Selecting customer details and calculating the total sales amount, then limiting to the top 5   
SELECT
    c.cust_id,
    c.cust_first_name || ' ' || c.cust_last_name AS customer_full_name,sh.countries.country_id as country_id  ,sh.countries.country_name,c.cust_city as city ,c.cust_state_province as state_probince ,
    c.cust_year_of_birth as year_of_birth, c.cust_gender as gender, c.cust_main_phone_number as phone_number,  
    SUM(s.amount_sold) AS total_sales_amount            -- Calculating total sales amount
FROM
    sh.sales s                                          -- Joining with the sales table
    JOIN sh.customers c ON s.cust_id = c.cust_id        -- Joining with the customers table
    join sh.countries on c.country_id =sh.countries.country_id -- Joining with the contries table
GROUP BY
    c.cust_id, customer_full_name,sh.countries.country_id   -- Grouping the result by customer id, full name and country id 
ORDER BY
    total_sales_amount DESC                             -- Sorting in descending order based on total sales amount
LIMIT 5;                                                -- Limiting the result to the top 5 customers

--This query selects customer details, calculates the total sales amount for each customer, and then retrieves the top five customers with the highest total sales amount.